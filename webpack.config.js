const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')

module.exports = {
    entry: "./src/index.js",
    output: {
        filename: 'index.js',
        path: path.join(__dirname,'build')
    },
    module : {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    },
    plugins: [
        new CopyPlugin({
            patterns: [{
                from: path.join(__dirname,'src'),
                globOptions: {
                    igonre:[ path.join(__dirname,'src','index.js')]
                }
            }]
        })
    ]
}