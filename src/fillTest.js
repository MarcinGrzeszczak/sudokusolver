const sudokuAPIURL = 'https://sugoku.herokuapp.com/board?difficulty=hard'
function getBoard() {
    fetch(sudokuAPIURL)
    .then(body => body.json())
    .then(data => {
        fillBoard(data.board)
        return getResult(data)
    })
    .catch(err => {
        console.log(err)
        alert('Error while getting board')
    })
}

async function getResult(data) {
    try {
        const encodeBoard = (board) => board.reduce((result, row, i) => result + `%5B${encodeURIComponent(row)}%5D${i === board.length -1 ? '' : '%2C'}`, '')
        const encodeParams = (params) => 
        Object.keys(params)
        .map(key => key + '=' + `%5B${encodeBoard(params[key])}%5D`)
        .join('&');

        const result = await fetch('https://sugoku.herokuapp.com/solve', {
        method: 'POST',
        body: encodeParams(data),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        })
        
        const jsonResult = await result.json()

        console.log(`Result:`)
        console.log(jsonResult)
        return Promise.resolve()
  }
  catch(e) {
    return Promise.reject(e)
  }
} 

function fillBoard(data) {
    let posCol = 0
    let posRow = 0
    let gridRow = 0
    let gridCol = 0

    console.log(data)
    for(const row of data) {
        posCol = 0
        gridCol = 0
        for(const col of row) {
            document.getElementById(`${gridRow},${gridCol}:${posRow},${posCol}`).value = col === 0? '':col
            posCol++
            if(posCol === 3){
                gridCol++
                posCol = 0
            }
        }
        posRow++
        if(posRow === 3){
            gridRow++
            posRow = 0
        }
    }
}

getBoard()
/* document.getElementById('0,0:1,0').value = 6
document.getElementById('0,0:1,1').value = 8
document.getElementById('0,0:2,0').value = 1
document.getElementById('0,0:2,1').value = 9



document.getElementById('0,1:0,0').value = 2
document.getElementById('0,1:0,1').value = 6
document.getElementById('0,1:1,1').value = 7
document.getElementById('0,1:2,2').value = 4


document.getElementById('0,2:0,0').value = 7
document.getElementById('0,2:0,2').value = 1
document.getElementById('0,2:1,1').value = 9
document.getElementById('0,2:2,0').value = 5


document.getElementById('1,0:0,0').value = 8
document.getElementById('1,0:0,1').value = 2
document.getElementById('1,0:1,2').value = 4
document.getElementById('1,0:2,1').value = 5


document.getElementById('1,1:0,0').value = 1
document.getElementById('1,1:1,0').value = 6
document.getElementById('1,1:1,2').value = 2
document.getElementById('1,1:2,2').value = 3


document.getElementById('1,2:0,1').value = 4
document.getElementById('1,2:1,0').value = 9
document.getElementById('1,2:2,1').value = 2
document.getElementById('1,2:2,2').value = 8


document.getElementById('2,0:0,2').value = 9
document.getElementById('2,0:1,1').value = 4
document.getElementById('2,0:2,0').value = 7
document.getElementById('2,0:2,2').value = 3


document.getElementById('2,1:0,0').value = 3
document.getElementById('2,1:1,1').value = 5
document.getElementById('2,1:2,1').value = 1
document.getElementById('2,1:2,2').value = 8


document.getElementById('2,2:0,1').value = 7
document.getElementById('2,2:0,2').value = 4
document.getElementById('2,2:1,1').value = 3
document.getElementById('2,2:1,2').value = 6
 */